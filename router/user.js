const { Router } = require('express');
const { getUsers, getUsersById, deleteUsersById, updateUsersById, createUser } = require('../controllers/user');
const router = Router();

router.get('/',getUsers)
router.get('/:id',getUsersById)
router.delete('/:id',deleteUsersById)
router.put('/:id',updateUsersById)
router.post('/',createUser)

module.exports = router