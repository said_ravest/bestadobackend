const { getUsers } = require("../controllers/user")
const User = require('../database/index')

const user = new User();

var id = ""

    describe("validar tipo de respuesta", () => {
        it("valida lista usuarios", async () => {
            const ValueReturn = await user.find()
            expect(Array.isArray(ValueReturn)).toBe(true)
        })
    })



    describe("validar tipo de respuesta", () => {
        it("valida creacion de usuario", async () => {
            const ValueReturn = await user.create({user:"prueba", username:"usuarioPrueba"})
            expect(Array.isArray(ValueReturn)).toBe(true)
        })
    })


    describe("validar usuarios ", () => {
        it("valida editar usuario", async () => {
            const find = await user.find()
            var id = find[0].id 
            const ValueReturn = await user.findByIdAndUpdate(id,{user:"prueba", username:"usuarioPrueba"})
            expect(Array.isArray(ValueReturn)).toBe(true)
        })
    })


    describe("validar usuarios ", () => {
        it("valida eliminar usuario", async () => {
            const find = await user.find()
            var id = find[0].id 
            const ValueReturn = await user.delete(id)
            expect(Array.isArray(ValueReturn)).toBe(true)
        })
    })

