require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();
const router = require('./router');

app.use(cors())
app.use(express.json())
app.use('/api',router)


app.listen(process.env.PORT,() => {
    console.log("Server corriendo en puerto", process.env.PORT);
}).on("error",(error) => {throw new Error("Error al iniciar el servidor",error)})