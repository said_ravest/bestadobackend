const User = require('../database/index')
const user = new User();

exports.getUsers = (req, res) => {
    const users = user.find()
    res.json(users)
}
exports.getUsersById = (req, res) => {
    id = req.params.id
    const users = user.findById(id)
    res.json(users)
}
exports.deleteUsersById = (req, res) => {
    id = req.params.id
    const users = user.delete(id)
    res.json(users)
}
exports.updateUsersById = (req, res) => {
    id = req.params.id
    body ={ "user": "Said", "username": "said.tattoo" }
    const users = user.findByIdAndUpdate(id, body)
    res.json(users)
}
exports.createUser = (req, res) => {
    body = req.body
    const users = user.create(body)
    res.json(users)
}
