const data = require('../data.json')
const fs = require('fs')
const { v4 }= require('uuid')
/**
 * dummy mongoDb
 **/
class User {
    
    find(){
        return data 
    }
    findById(id){
        return data.find(user => user.id === id)
    }
    findByIdAndUpdate(id, body){
       data.map((dato)=> {
            if(dato.id ===id){
                dato.user = body.user
                dato.username = body.username
            }
        })
        fs.writeFileSync(`./data.json`,JSON.stringify(data, null, 4),(error) => {
            if(error) throw new Error(error)
        })
        return data
    }

    create(body){
        body.id = v4()
        data.push(body) 
        fs.writeFileSync(`./data.json`,JSON.stringify(data, null, 4),(error) => {
            if(error) throw new Error(error)
        })
        return data
    }

    delete(id){
       
        const userList = data.filter(user => user.id !== id)
        fs.writeFileSync(`./data.json`,JSON.stringify(userList,null,4),(error) => {
            if(error) throw new Error(error)
        })
        return userList
    }

}
module.exports = User;
